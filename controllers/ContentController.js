const contentService = require('../services/ContentService');
const Boom = require('@hapi/boom');
const uniqid = require('uniqid');

class ContentConroller {
    constructor(contentService) {
        this.contentService = contentService;
    }

    async getOne(request, h) {
        try {
            const result = await this.contentService.getOne(request.params.id);
            if (result === null) {
                Boom.notFound(`content ${request.params.id} not found`);
            }
            return JSON.parse(result);
        } catch (err) {
            Boom.badImplementation(err)
        }
    }

    async getMany(request, h) {
        try {
            const result = await this.contentService.getMany();
            if (result === null) {
                return h.response().code(204)
            } else {
                return Object.values(result).map(val => JSON.parse(val));
            }
        } catch (err) {
            Boom.badImplementation(err)
        }
    }

    async create(request, h) {
        try {
            const fields = {
                title: request.payload.title, 
                body: request.payload.body
            };
            const id = uniqid();
            const created = Math.floor(Date.now()/1000);
            const payload = { id: id , created: created, ...fields };
            await this.contentService.create(id, JSON.stringify(payload));
            return h.response({ id }).code(201);
        } catch (err) {
            Boom.badImplementation(err)
        }
    }

    async replace(request, h) {
        try {
            const fields = {
                title: request.payload.title, 
                body: request.payload.body
            };
            const created = Math.floor(Date.now()/1000);
            const payload = { id: request.params.id , created: created, ...fields };
            await this.contentService.create(
                request.params.id,
                JSON.stringify(payload)
            );
            return payload;
        } catch (err) {
            Boom.badImplementation(err);
        }
    }

    async edit(request, h) {
        try {
            const existing = await this.contentService.getOne(request.params.id);
            const fields = Object.keys(request.payload).filter(
                key => !['id', 'created'].includes(key)
            ).reduce((acc, curr) => {
                return { ...acc, [curr]: request.payload[curr] }
            }, {});

            const merge = { ...JSON.parse(existing), ...fields };
            const created = Math.floor(Date.now()/1000);
            const payload = { id: request.params.id , ...merge, created: created };
            
            await this.contentService.create(
                request.params.id,
                JSON.stringify(payload)
            );
            return payload;
        } catch (err) {
            Boom.badImplementation(err)
        }
    }

    async delete(request, h) {
        try {
            const result = await this.contentService.delete(request.params.id);
            const message = result 
            ? `content ${request.params.id} has been deleted`
            : `content ${request.params.id} has not been deleted`
            return { message };
        } catch (err) {
            Boom.badImplementation(err)
        }
    }
}

const bootstrap = () => {
    const controller = new ContentConroller(contentService);
    return [
        { 
            method: 'GET', 
            path: '/contents',
            config: {
                auth: {
                    strategy: 'jwt',
                    scope: ['admin']
                },
                handler: (request, h) => controller.getMany(request, h)
            }
        },
        { 
            method: 'GET', 
            path: '/contents/{id}',
            handler: (request, h) => controller.getOne(request, h)
        },
        { 
            method: 'POST', 
            path: '/contents',
            handler: (request, h) => controller.create(request, h)
        },
        { 
            method: 'PUT', 
            path: '/contents/{id}',
            handler: (request, h) => controller.replace(request, h)
        },
        { 
            method: 'PATCH', 
            path: '/contents/{id}',
            handler: (request, h) => controller.edit(request, h)
        },
        { 
            method: 'DELETE', 
            path: '/contents/{id}',
            handler: (request, h) => controller.delete(request, h)
        }
    ]
}
module.exports = bootstrap();