const validate = async(payload, request, h) => {
    if (payload.roles) {
        const { roles, ...infos } = payload;
        const credentials = { ...infos, scope: roles };
        return { isValid: true,  credentials };
    } else {
        return { isValid: false };
    }
}

module.exports = validate;