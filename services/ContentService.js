const redisService = require('./RedisService');

class ContentService {
    constructor(redisService) {
        this.redisService = redisService;
        this.contentSet = 'contents';
    }

    async getOne(key){
        return await this.redisService.hget(this.contentSet, key);
    }

    async getMany(){
        return await this.redisService.hgetall(this.contentSet);
    }

    async create(key, value){
        return await this.redisService.hset(this.contentSet, key, value);
    }

    async delete(key){
        return await this.redisService.hdel(this.contentSet, key);
    }
}

module.exports = new ContentService(redisService);