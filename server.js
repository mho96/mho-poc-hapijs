'use strict';

const Hapi = require('@hapi/hapi');
const contentRoutes = require('./controllers/ContentController');
const config = require('./config.json');
const validate = require('./policies/auth.policy');
const authJwtPlugin = require('hapi-auth-jwt2');

const init = async() => {
    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });

    await server.register(authJwtPlugin);

    const { secret } = config; // a ne pas reproduire à la maison :D
    server.auth.strategy('jwt', 'jwt', { 
        key: secret,
        validate,
        verifyOptions: {
            algorithms: [ 'HS256' ]
        }
    });
  
    server.route(contentRoutes);

    await server.start();
    console.log('server running at %s', server.info.uri);
}

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
})

init();